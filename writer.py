#! /usrr/bin/env python

from __future__ import division, print_function


import numpy as np
import argparse


n_samples = 4096
trigger_source = 3 # external, rising
trigger_rate_internal = 1000 # seems to be ignored for trigger_source = 3
	
def write_DS345(volt_high_mV, ramp_time_ms, off_time_ms, out_file, ramp_type):
	total_time_ms = ramp_time_ms + off_time_ms
	sample_rate_hz = int(n_samples / (1e-3 * total_time_ms))

	n_samples_ramp = int((ramp_time_ms / total_time_ms) * n_samples)

	out_vals = np.zeros((n_samples,))


	ramp_down_dict = {}
	ramp_down_dict['sin'] = 1e-3 * volt_high_mV * (0.5 + 0.5 * np.cos(np.linspace(0, np.pi, n_samples_ramp)))
	ramp_down_dict['lin'] = np.linspace(1e-3 * volt_high_mV, 0, n_samples_ramp)

        if ramp_type not in ramp_down_dict.keys():
            raise argparse.ArgumentTypeError("Argument --ramp must be one of {}".format(ramp_down_dict.keys()))

	ramp_down = ramp_down_dict[ramp_type]
	ramp_up = ramp_down[::-1]

	out_vals[:n_samples_ramp] = ramp_down
	out_vals[n_samples-n_samples_ramp:] = ramp_up

	with open(out_file, 'w') as fout:
		fout.write("{}\n".format(n_samples))
		fout.write("{}\n".format(sample_rate_hz))
		fout.write("{}\n".format(trigger_source))
		fout.write("{}\n".format(trigger_rate_internal))

		for val in out_vals:
			fout.write("{:.6f}\n".format(val))


if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="""Write a waveform file to be read by the SRS DS345 AWG. The waveform will:\n
		- start high, at a specified voltage volt_high_mV,\n
		- ramp down to 0 mV when a trigger is received, over a span of ramp_time_ms, and\n
		- ramp back up to volt_high_mv after off_time_ms has elapsed.\n""")
	parser.add_argument("volt_high_mV", type=int, help="High waveform voltage, in mV")
	parser.add_argument("ramp_time_ms", type=float, help="Time taken to ramp the waveform down and up, in ms")
	parser.add_argument("off_time_ms", type=float, help="Time between the start of the ramp down and the start of the ramp back up, in ms")
	parser.add_argument("out_file", help="Location to which to write the output file")
	parser.add_argument("--ramp", dest="ramp_type", default="lin", help="Type of ramp. Must be either 'lin' or 'sin'")

	args = parser.parse_args()

	write_DS345(args.volt_high_mV, args.ramp_time_ms, args.off_time_ms, args.out_file, args.ramp_type)
